let return x = Lwt.return (Ok x)

let error = Lwt.return (Stdlib.Error ())

let ( >>= ) x f =
  Lwt.bind x (function Error e -> Lwt.return (Error e) | Ok y -> f y)

let io_counter = ref 0

let reset_io_counter () = io_counter := 0

let io =
  fun _ ->
  incr io_counter;
  return ()


module FreeMonad = struct

  type _ command = CommandIO : Z.t -> unit command

  type _ t =
    | Request : 'a command * ('a -> 'b t) -> 'b t
    | Return : 'a -> 'a t
    | Error

  let return x = Return x

  let rec ( >>= ) x f =
    match x with
    | Return x -> f x
    | Error -> Error
    | Request (c, g) -> Request (c, fun x -> g x >>= f)

  let request c f = Request (c, f)

end
