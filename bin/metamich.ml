(*---------------------------------------------------------------------------

  Helpers

----------------------------------------------------------------------------*)

open Metamichelsonlib.Metahelper

let n =
  if Array.length Sys.argv > 1 then
    int_of_string Sys.argv.(1)
  else
    100

let amplify n f =
  let y = f () in
  for _x = 1 to n do
    ignore (f ())
  done;
  y

let factor =
  if Array.length Sys.argv > 2 then
    int_of_string Sys.argv.(2)
  else
    500000

type z = Z.t

(*---------------------------------------------------------------------------

  OCaml implementations

----------------------------------------------------------------------------*)

let fact n =
  let rec aux k accu =
    if Z.(compare k zero = 0) then return accu
    else
      let accu = Z.mul accu k in
      io accu >>= fun () ->
      aux (Z.sub k (Z.of_int 1)) accu
  in
  aux n (Z.of_int 1)

let ocaml_eval () =
  Lwt_main.run (Lwt.bind (fact (Z.of_int n)) (function Ok z -> Lwt.return z | _ -> assert false))

let factn, io_counts =
  let y = ocaml_eval () in
  (y, !io_counter)

let reference_time = ref None
let bench (what, how) = try
    let start = Unix.gettimeofday () in
    let factn', io_counts' =
      amplify factor @@ fun () ->
                        reset_io_counter ();
                        let y = how () in
                        (y, !io_counter)
    in
    let stop = Unix.gettimeofday () in
    let score = ((stop -. start) *. 1000000. /. float_of_int factor) in
    let ratio = match !reference_time with
      | None -> reference_time := Some score; 1.
      | Some rscore -> score /. rscore
    in
    let success = Z.compare factn factn' = 0 && io_counts = io_counts' in
    Printf.printf "[%s] %s: %f µs [x %f]\n"
      ( if success then "OK" else "KO" )
      ( if success then what else (what ^ Printf.sprintf " : %s <> %s || %d <> %d"
                                            (Z.to_string factn) (Z.to_string factn')
                                            io_counts io_counts'))
      score
      ratio
  with e -> Printf.printf "[KO] %s: %s\n" what (Printexc.to_string e)

let time f =
  let start = Unix.gettimeofday () in
  let y = f () in
  let stop = Unix.gettimeofday () in
  y, stop -. start

(*---------------------------------------------------------------------------

  OCaml implementations

----------------------------------------------------------------------------*)

(*---------------------------------------------------------------------------

  Interpreters over the standard instruction datatype

----------------------------------------------------------------------------*)

module StandardInstr = struct
  type (_, _) instr =
    | Push : 'b -> ('a, 'b * 'a) instr
    | Mul : (z * (z * 'a), z * 'a) instr
    | Dec : (z * 'a, z * 'a) instr
    | CmpNZ : (z * 'a, bool * 'a) instr
    | Loop : ('a, bool * 'a) instr -> (bool * 'a, 'a) instr
    | Seq : ('a, 'b) instr * ('b, 'c) instr -> ('a, 'c) instr
    | Dup : ('a * 's, 'a * ('a * 's)) instr
    | Swap : ('a * ('b * 's), 'b * ('a * 's)) instr
    | Drop : ('a * 's, 's) instr
    | Dip : ('s, 't) instr -> ('a * 's, 'a * 't) instr
    | IO : (z * 'a, z * 'a) instr

  type bottom = unit

  let ( @ ) s1 s2 = Seq (s1, s2)

  let fact n =
    assert (Z.(n > zero));
    Push n @ Dup
    @ Push (Z.of_int 1)
    @ Dup @ CmpNZ @ Dip Swap
    @ Loop (Dup @ Dip Swap @ Mul @ IO @ Swap @ Dec @ Dup @ CmpNZ)
    @ Drop

  let factn = fact (Z.of_int n)

  (*---------------------------------------------------------------------------

      An interpreter in the Lwt monad (as in the current implementation)

    ----------------------------------------------------------------------------*)

  module Monadic = struct
    type 'a t = ('a, unit) result Lwt.t

    let return x = Lwt.return (Ok x)

    let ( >>= ) x f =
      Lwt.bind x (function Error e -> Lwt.return (Error e) | Ok y -> f y)

    let rec step : type a b. (a, b) instr -> a -> b t =
      fun instr stack ->
      match (instr, stack) with
      | Push x, stack -> return (x, stack)
      | Mul, (x, (y, stack)) -> return (Z.mul x y, stack)
      | Dec, (x, stack) -> return (Z.(sub x (of_int 1)), stack)
      | CmpNZ, (x, stack) -> return (Z.(compare x zero <> 0), stack)
      | Loop _, (false, stack) -> return stack
      | Loop body, (true, stack) ->
         step body stack >>= fun stack -> step (Loop body) stack
      | Seq (i1, i2), stack -> step i1 stack >>= fun stack -> step i2 stack
      | Dup, (x, stack) -> return (x, (x, stack))
      | Swap, (x, (y, stack)) -> return (y, (x, stack))
      | Drop, (_, stack) -> return stack
      | Dip i, (x, stack) -> step i stack >>= fun stack -> return (x, stack)
      | IO, (z, _) -> Lwt.bind (io z) (fun _ -> return stack)

    let eval () =
      Lwt_main.run
      @@ Lwt.bind (step factn ()) (function
             | Ok (z, _) -> Lwt.return z
             | Error _ -> failwith "runtime error")
  end

  module Meta = struct

    open Codelib
    open Metamichelsonlib.Metahelper

    let () =
      List.iter Runnative.add_search_path
        [
          "/home/yann/.opam/4.11.1+BER/lib/metamichelson/lib";
          "/home/yann/.opam/4.11.1+BER/lib/zarith";
          "/home/yann/.opam/4.11.1+BER/lib/lwt"
        ]

    let _ = Metamichelsonlib.Metahelper.return

    type 'a t = ('a, unit) result Lwt.t

    let rec step : type a b. (a, b) instr -> a code -> (b t) code =
      fun instr ->
      match instr with
      | Push x ->
         fun stack -> .< return (x, .~stack) >.
      | Mul ->
         fun stack -> .<
                     let (x, (y, stack)) = .~stack in
                     return (Z.mul x y, stack)
                   >.
      | Dec ->
         fun stack -> .<
                     let (x, stack) = .~stack in
                     return (Z.(sub x (of_int 1)), stack)
                   >.
      | CmpNZ ->
         fun stack -> .<
                     let (x, stack) = .~stack in
                     return (Z.(compare x zero <> 0), stack)
                   >.
      | Loop body ->
         let body_code =
           step body
         in
         fun stack -> .<
                     let rec loop = fun stack ->
                       let (b, stack) = stack in
                       if b then
                         .~(body_code .< stack >.) >>= loop
                       else
                         return stack
                     in
                     loop .~stack
                   >.
      | Seq (i1, i2) ->
         fun stack ->
         .<
           .~(step i1 stack) >>= fun stack -> .~(step i2 .<stack>.)
         >.
      | Dup ->
         fun stack ->
         .<
           let (x, stack) = .~stack in
           return (x, (x, stack))
         >.
      | Swap ->
         fun stack ->
         .<
           let (x, (y, stack)) = .~stack in
           return (y, (x, stack))
         >.
      | Drop ->
         fun stack ->
         .<
           let  (_, stack) = .~stack in
           return stack
         >.
      | Dip i ->
         fun stack ->
         .<
           let (x, stack) = .~stack in
           .~(step i .<stack>.) >>= fun stack -> return (x, stack)
         >.
      | IO ->
         fun stack ->
         .<
           let (z, _) = .~stack in
           Lwt.bind (io z) (fun _ ->  return .~stack)
         >.

    let string_of_code c =
      let () = Codelib.print_code Format.str_formatter c in
      Format.flush_str_formatter ()

    let code = .< fun () -> .~(step factn .< () >.) >.

    let fact = Runnative.run code

    let eval () =
      Lwt_main.run
      @@ Lwt.bind (fact ()) (function
             | Ok (z, _) -> Lwt.return z
             | Error _ -> failwith "runtime error")

  end

end

module DCPS_AStack = struct

  type loop = Loop

  type dip = Dip

  type (_, _, _, _) instr =
    | KHalt : ('r, 'f, 'r, 'f) instr
    | KPush : 'a * ('a, 'b * 's, 'r, 'f) instr -> ('b, 's, 'r, 'f) instr
    | KMul : (z, 's, 'r, 'f) instr -> (z, (z * 's), 'r, 'f) instr
    | KDec : (z, 's, 'r, 'f) instr -> (z, 's, 'r, 'f) instr
    | KCmpNZ : (bool, 's, 'r, 'f) instr -> (z, 's, 'r, 'f) instr
    | KLoop : ('a, 's, bool, 'a * 's) instr * ('a, 's, 'r, 'f) instr -> (bool, 'a * 's, 'r, 'f) instr
    | KDup : ('a, ('a * 's), 'r, 'f) instr -> ('a, 's, 'r, 'f) instr
    | KSwap : ('b, ('a * 's), 'r, 'f) instr -> ('a, ('b * 's), 'r, 'f) instr
    | KDrop : ('b, 's, 'r, 'f) instr -> ('a, 'b * 's, 'r, 'f) instr
    | KDip : ('b, 's, 'c, 't) instr * ('a, 'c * 't, 'r, 'f) instr -> ('a, 'b * 's, 'r, 'f) instr
    | KIO : (z, 'a, 'r, 'f) instr -> (z, 'a, 'r, 'f) instr

  and (_, _, _, _) cont =
    | KNil : ('r, 'f, 'r, 'f) cont
    | KCons : ('a, 's, 'b, 't) instr * ('b, 't, 'r, 'f) cont -> ('a, 's, 'r, 'f) cont
    | KUnDip : 'b
               * ('b, 'a * 's, 'b, 't) instr
               * ('b, 't, 'r, 'f) cont -> ('a, 's, 'r, 'f) cont
    | KLoopIn : ('a, 's, bool, 'a * 's) instr
                * ('a, 's, 'r, 'f) cont -> (bool, 'a * 's, 'r, 'f) cont


  type bottom = unit

  let push n k = KPush (n, k)

  let dup k = KDup k

  let mul k = KMul k

  let dec k = KDec k

  let cmpnz k = KCmpNZ k

  let loop i k = KLoop (i, k)

  let swap k = KSwap k

  let drop k = KDrop k

  let dip i k = KDip (i, k)

  let kio k = KIO k

  let ( !! ) i = i KHalt

  let fact n =
    assert (Z.(n > zero));
    push n @@ dup
    @@ push (Z.of_int 1)
    @@ dup @@ cmpnz @@ dip !!swap
    @@ loop
         (dup @@ dip !!swap @@ mul @@ kio @@ swap @@ dec @@ dup @@ !!cmpnz)
         !!drop

  let factn = fact (Z.of_int n)

  module SmallStep = struct

    type 'a ret = ('a, unit) result Lwt.t

    let step : type a s r f. (a, s, r, f) instr -> a -> s -> (r * f) ret =
      fun i a stack ->
      let rec exec : type a s b t. (a, s, b, t) instr -> (b, t, r, f) cont -> a -> s -> (r * f) ret =
        fun k ks a s ->
        match (k, ks) with
        | KHalt, KNil -> return (a, s)
        | KHalt, KLoopIn (ki, ks') ->
           let (a', s) = s in
           if a then
             exec ki ks a' s
           else
             exec KHalt ks' a' s
        | KHalt, KCons (k, ks) -> exec k ks a s
        | KHalt, KUnDip (x, k, ks) -> exec k ks x (a, s)
        | KIO k, ks ->
           (io a) >>= (fun _ -> exec k ks a s)
        | KPush (z, k), ks -> exec k ks z (a, s)
        | KLoop (ki, k), ks -> (
          exec KHalt (KLoopIn (ki, KCons (k, ks))) a s)
        | KMul k, ks ->
           let (b, s) = s in
           exec k ks (Z.mul a b) s
        | KDec k, ks ->
           exec k ks (Z.sub a (Z.of_int 1)) s
        | KCmpNZ k, ks ->
           exec k ks (Z.(compare a zero) <> 0) s
        | KDup k, ks ->
           exec k ks a (a, s)
        | KSwap k, ks ->
           let (b, s) = s in
           exec k ks b (a, s)
        | KDrop k, ks ->
           let b, s = s in
           exec k ks b s
        | KDip (ki, k), ks ->
           let b, s = s in
           exec ki (KCons (KPush (a, k), ks)) b s
      in
      exec i KNil a stack

    let eval () =
      Lwt_main.run (Lwt.bind (step factn () ())
                      (function
                       | Ok (z, _) -> Lwt.return z
                       | _ -> assert false))

  end

  module CPS_BigStep = struct

    type 'a ret = ('a, unit) result Lwt.t

    let step : type a s r f. (a, s, r, f) instr -> a -> s -> (r * f) ret =
      fun i a stack ->
      let rec exec
      : type a s b t.
        (a, s, b, t) instr -> (b -> t -> (r * f) ret) -> a -> s -> (r * f) ret =
        fun i k a s ->
        match i with
        | KHalt ->
           k a s
        | KIO i ->
           (io a) >>= (fun _ -> exec i k a s)
        | KPush (z, i) ->
           exec i k z (a, s)
        | KLoop (body, exit_loop) -> (
          if a then
            exec body (fun b t -> exec i k b t) (fst s) (snd s)
          else
            exec exit_loop k (fst s) (snd s)
        )
        | KMul i ->
           let (b, s) = s in
           exec i k (Z.mul a b) s
        | KDec i ->
           exec i k (Z.sub a (Z.of_int 1)) s
        | KCmpNZ i ->
           exec i k (Z.(compare a zero) <> 0) s
        | KDup i ->
           exec i k a (a, s)
        | KSwap i ->
           let (b, s) = s in
           exec i k b (a, s)
        | KDrop i ->
           let b, s = s in
           exec i k b s
        | KDip (body, i) ->
           let b, s = s in
           exec body (fun c u -> exec i k a (c, u)) b s
      in
      exec i (fun a s -> return (a, s)) a stack

    let eval () =
      Lwt_main.run (Lwt.bind (step factn () ()) (function Ok (z, _) -> Lwt.return z | _ -> assert false))

  end

  module Meta = struct

    open Codelib
    open Metamichelsonlib.Metahelper

    let () =
      List.iter Runnative.add_search_path
        [
          "/home/yann/.opam/4.11.1+BER/lib/metamichelson/lib";
          "/home/yann/.opam/4.11.1+BER/lib/zarith";
          "/home/yann/.opam/4.11.1+BER/lib/lwt"
        ]

    let _ = Metamichelsonlib.Metahelper.return

    type 'a ret = ('a, unit) result Lwt.t

    let compile : type a s r f. (a, s, r, f) instr -> a code -> s code -> (r * f) ret code =
      fun i a stack ->
      let rec translate
      : type a s b t.
        (a, s, b, t) instr -> (b -> t -> (r * f) ret) code -> a code -> s code -> (r * f) ret code =
        fun i ->
        match i with
        | KHalt -> fun k a s ->
          .<
             .~k .~a .~s
           >.
        | KIO i -> fun k a s ->
          .<
             let a' = .~a in
             (io a') >>= (fun _ -> .~(translate i k .< a' >. s))
           >.
        | KPush (z, i) -> fun k a s ->
          .<
             .~(translate i k .< z >. .< (.~a, .~s) >. )
          >.
        | KLoop (body, exit_loop) -> fun k a s -> (
          let body_code = translate body in
          let exit_code = translate exit_loop in
          .<
            let rec loop a s =
              if a then
                let a', s' = s in
                 .~(body_code .< loop >. .< a' >. .< s' >.)
              else
                let a', s' = s in
                 .~(exit_code k .< a' >. .< s' >.)
            in
            loop .~a .~s
          >.
        )
        | KMul i -> fun k a s ->
         .<
           let (b, s) = .~s in
           .~(translate i k .< (Z.mul .~a b) >. .< s >.)
          >.
       | KDec i -> fun k a s ->
          .<
             .~(translate i k .< (Z.sub .~a (Z.of_int 1)) >. s)
          >.
       | KCmpNZ i -> fun k a s ->
          .<
             .~(translate i k .< (Z.(compare .~a zero) <> 0) >. s)
          >.
       | KDup i -> fun k a s ->
           .<
             let a = .~a in
             .~(translate i k .< a >. .< (a, .~s) >.)
           >.
       | KSwap i -> fun k a s ->
           .<
             let (b, s) = .~s in
             .~(translate i k .< b >. .< (.~a, s) >.)
           >.
       | KDrop i -> fun k _ s ->
           .<
             let b, s = .~s in
             .~(translate i k .< b >. .< s >.)
           >.
       | KDip (body, i) -> fun k a s ->
          .<
             let b, s = .~s in
             .~(translate body .< (fun c u -> .~(translate i k a .< (c, u) >. )) >. .< b >. .< s >.)
          >.
      in
      translate i .< (fun a s -> return (a, s)) >. a stack

    let string_of_code c =
      let () = Codelib.print_code Format.str_formatter c in
      Format.flush_str_formatter ()

    let _ =
      let _, time =
        time @@ fun () ->
                let code = .< fun () -> .~(compile factn .< () >. .< () >.) >. in
                let _fact = Runnative.run code in
                ()
      in
      Printf.eprintf "Compilation time: %f\n" time

    let code = .< fun () -> .~(compile factn .< () >. .< () >.) >.

    let _ = Printf.eprintf "%s\n" (string_of_code code)

    let fact = Runnative.run code

    let eval () =
      Lwt_main.run
      @@ Lwt.bind (fact ()) (function
             | Ok (z, _) -> Lwt.return z
             | _ -> assert false)

  end


end

let () =
  List.iter bench
    [
      ("OCaml", ocaml_eval);
      ("StandardInstr.Monadic.eval", StandardInstr.Monadic.eval);
      ("StandardInstr.Meta.eval", StandardInstr.Meta.eval);
      ("DCPS_AStack.SmallStep.eval", DCPS_AStack.SmallStep.eval);
      ("DCPS_AStack.CPS_BigStep.eval", DCPS_AStack.CPS_BigStep.eval);
      ("DCPS_AStack.Meta.eval", DCPS_AStack.Meta.eval)
    ]
